#!/bin/bash

name=$(python3 -c "print('%s.fna' % '$1'.split('/')[-1][:-7])")
gunzip $1 -c > /bbx/tmp/$name
