import os, sys
name = sys.argv[1].split('/')[-1][:-3]
for line in open('/bbx/input/training/mapping.tsv'):
    if line.startswith('#') or line.startswith('@'):
        continue
    if not line.strip().split('\t')[4] == name:
        continue
    taxid = line.strip().split('\t')[0]
    inp = '/bbx/tmp/%s.fa' % name
    # edit the header as follow >id|kraken:taxid|xxxxx
    if os.path.exists('/bbx/tmp/input/%s.fa' % name):
        continue
    outp = open('/bbx/tmp/input/%s.fa' % name, 'w')
    #print('1 %s %s' % (name, inp))
    for line2 in open(inp, 'r'):
        if line2.startswith('>'):
            line2 = '%s|kraken:taxid|%s\n' % (line2.strip().split(' ')[0], taxid)
        outp.write(line2)
    outp.close()
    #print('2 %s %s' % (name, inp))
    os.remove(inp) # save disk space
