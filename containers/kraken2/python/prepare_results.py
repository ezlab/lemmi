#
outp=open('/bbx/output/profile.tsv','w')
all_abu={}
ranks_header = '@Ranks:superkingdom|phylum|class|order|family|genus|species\n'
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:kraken\n')
outp.write('@Version:0.9.1\n')
outp.write(ranks_header)
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
ranks = {}
rank_name = {'S': 'species', 'O': 'order', 'G': 'genus', 'F': 'family', 'C': 'class', 'P': 'phylum', 'K': 'kingdom', 'D': 'domain'}
for line in open('/bbx/tmp/tmp.out.report'):
    id=line.split('\t')[4]
    abu=float(line.split('\t')[0])
    rank=line.split('\t')[3]
    ranks.update({id: rank})
    if id in all_abu:
        all_abu[id] += abu
    else:
        all_abu.update({id: abu})
for i in all_abu:
    try:
        outp.write('%s\t%s\t%s\t%s\t%s\n' % (i, rank_name[ranks[i]], i, i, all_abu[i]))
    except KeyError:
        pass
outp.close()
outp=open('/bbx/output/bins.tsv','w')
outp.write('# Taxonomic Binning Output\n')
outp.write('@SampleID:kbraken\n')
outp.write('@Version:0.9.0\n')
outp.write(ranks_header)
outp.write('@@SEQUENCEID\tBINID\n')
for line in open('/bbx/tmp/tmp.out'):
    if line.startswith('C'):
        outp.write('%s\t%s\n' % (line.strip().split('\t')[1],line.strip().split('\t')[2]))
outp.close()
