Code for building the containers included on https://lemmi.ezlab.org/

You can obtain the containers corresponding to a specific ranking fingerprint by retrieving the corresponding tag, e.g. beta01.20190325

- *demo_kraken*: container to use as basis for creating your own. Self documented tutorial. See also https://gitlab.com/ezlab/lemmi/wikis/user-guide
- *metacache05_forkmer31*: metacache v0.5.0, built to allow k-mer values up to 31
- *metacache05_forkmer16*: metacache v0.5.0, built to allow k-mer values up to 16 (less memory consuming)
- *kaiju*: Kaiju 1.6.0
- *kraken2*: Kraken 2.0.7
- *bracken2*: Kraken 2.0.7 + bracken 2.0
- *bracken1*: Kraken 1.1 + bracken 2.0
- *metaphlan2*: Metaphlan 2.7.7
- *centrifuge*: Centrifuge 1.0.3
- *ganon*: Ganon, commit 3a0b87fcda1f47455b4eefc6c47989b0c601f16f
- *ccmetagen*: CCMetagen, commit a5562ae
- *clark-l*: CLARK V1.2.6.1