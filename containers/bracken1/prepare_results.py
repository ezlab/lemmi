#
outp=open('/bbx/output/profile.tsv','w')
all_abu={}
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:bracken\n')
outp.write('@Version:0.9.1\n')
outp.write('@Ranks:superkingdom|phylum|class|order|family|genus|species\n')
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
ranks = {}
rank_name = {'S': 'species', 'O': 'order', 'G': 'genus', 'F': 'family', 'C': 'class', 'P': 'phylum', 'K': 'kingdom', 'D': 'domain'}
for line in open('/bbx/tmp/report.bracken.denorm'):
    if line.startswith('name'):
        continue
    id=line.split('\t')[1]
    abu=float(line.split('\t')[6])
    rank=line.split('\t')[2]
    ranks.update({id: rank})
    if id in all_abu:
        all_abu[id] += abu
    else:
        all_abu.update({id: abu})
for i in all_abu:
    try:
        outp.write('%s\t%s\t%s\t%s\t%s\n' % (i, rank_name[ranks[i]], i, i, all_abu[i]))
    except KeyError:
        pass
outp.close()
outp=open('/bbx/output/bins.tsv','w')
outp.write('# Taxonomic Binning Output\n')
outp.write('@SampleID:bracken\n')
outp.write('@Version:0.9.0\n')
outp.write('@Ranks:superkingdom|phylum|class|order|family|genus|species\n')
outp.write('@@SEQUENCEID\tBINID\n')
for line in open('/bbx/tmp/tmp.out'):
    if line.startswith('C'):
        outp.write('%s\t%s\n' % (line.strip().split()[1],line.strip().split()[2]))
outp.close()
