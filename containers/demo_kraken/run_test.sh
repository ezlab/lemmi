u=${1?Need a number for the userid}
echo "user id is "$u
echo "delete the bbx folder..."
rm -r bbx
echo "checkout a fresh bbx folder..."
git checkout bbx
echo "build_ref..."
docker run -e thread=1 -e taxlevel=genus -e reference=myref -e parameter=8,4 -v $(pwd)/bbx/input/:/bbx/input:ro -v $(pwd)/bbx/output/:/bbx/output:rw -v $(pwd)/bbx/tmp/:/bbx/tmp:rw -v $(pwd)/bbx/metadata/:/bbx/metadata:rw -v $(pwd)/bbx/reference/:/bbx/reference:rw -u $u kraken build_ref
echo "delete the content of bbx/tmp..."
rm -r bbx/tmp/*
echo "delete the content of bbx/input/training/ except the dmp files..."
rm bbx/input/training/*.gz
rm bbx/input/training/mapping.*
mv bbx/metadata/log.txt bbx/metadata/log_build.txt
echo "analysis"
docker run -e thread=1 -e taxlevel=genus -e reference=myref -v $(pwd)/bbx/input/:/bbx/input:ro -v $(pwd)/bbx/output/:/bbx/output:rw -v $(pwd)/bbx/tmp/:/bbx/tmp:rw -v $(pwd)/bbx/metadata/:/bbx/metadata:rw -v $(pwd)/bbx/reference/:/bbx/reference:ro -u $u kraken analysis
echo "logs in bbx/metadata/:"
ls -l bbx/metadata/
echo "expected output files:"
ls bbx/output/bins.genus.tsv
ls bbx/output/profile.genus.tsv
