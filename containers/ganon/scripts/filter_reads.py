outp=open('/bbx/tmp/reads.1.clean.fq', 'w')
n=0
for line1, line2 in zip(open('/bbx/tmp/reads.1.fq'), open('/bbx/tmp/reads.2.fq')):
    n+=1
    if n % 4 == 2:
        outp.write('%sN%s\n'% (line1.strip().replace('B', 'N').replace('U', 'N').replace('R', 'N').replace('Y', 'N').replace('S', 'N').replace('W', 'N').replace('K', 'N').replace('M', 'N').replace('D', 'N').replace('H', 'N').replace('V', 'N'),line2.strip().replace('B', 'N').replace('U', 'N').replace('R', 'N').replace('Y', 'N').replace('S', 'N').replace('W', 'N').replace('K', 'N').replace('M', 'N').replace('D', 'N').replace('H', 'N').replace('V', 'N')))
    elif n % 4 == 0:
        outp.write('%s!%s\n' % (line1.strip(),line2.strip()))
    else:
        outp.write(line1)
