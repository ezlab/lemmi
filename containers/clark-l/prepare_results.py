import sys
#
rank = sys.argv[1]
outp=open('/bbx/output/profile.tsv','w')
all_abu={}
ranks_header = open('/bbx/input/training/mapping.tsv').readlines()[2]
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:clark\n')
outp.write('@Version:0.9.1\n')
outp.write(ranks_header)
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
for line in open('/bbx/tmp/profile.tsv'):
    taxid=line.split('\t')[0]
    abu=float(line.split('\t')[1])
    outp.write('%s\t%s\t%s\t%s\t%s\n' % (taxid, rank, taxid, taxid, abu))
outp=open('/bbx/output/bins.tsv','w')
outp.write('# Taxonomic Binning Output\n')
outp.write('@SampleID:clark-s\n')
outp.write('@Version:0.9.0\n')
outp.write(ranks_header)
outp.write('@@SEQUENCEID\tBINID\n')
for line in open('/bbx/tmp/bench.csv'):
    if not line.startswith('Object') and not line.strip().split(',')[2] == 'NA':
        outp.write('%s\t%s\n' % (line.strip().split(',')[0],line.strip().split(',')[2]))
outp.close()
