to_find = [line.strip() for line in open('/bbx/tmp/names').readlines()]
name_dmp_file = open('/bbx/reference/names.dmp')
outp = open('/bbx/tmp/taxo_mapping.tsv', 'w')
for line in name_dmp_file:
    if line.replace('\t','').split('|')[1] in to_find:
        outp.write('%s\t%s\n' % (line.replace('\t','').split('|')[1],line.replace('\t','').split('|')[0]))

