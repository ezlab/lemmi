import re
import os
outp = open('/bbx/tmp/train.faa', 'w')
regex = re.compile('[^ACDEFGHIKLMNPQRSTVWY]')
for entry in open('/bbx/input/training/mapping.tsv'):
    if entry.startswith('#') or entry.startswith('@'):
        continue
    taxid = int(entry.split('\t')[0])
    f = '%s.faa' %  entry.strip().split('\t')[4]
    i = 0
    for line in open('/bbx/tmp/%s' % (f)):
    #edit the header as follow >taxid and clean letters not in the 20 AA
        if line.startswith('>'):
            i += 1
            line = '>%i_%s\n' % (i, taxid)
        else:
            line = '%s\n' % regex.sub('', line)
        outp.write(line)
    os.remove('/bbx/tmp/%s' % (f))
outp.close()
