#
import sys
from ete3 import NCBITaxa 
ncbi = NCBITaxa()
level = sys.argv[1]
outp=open('/bbx/output/profile.%s.tsv' % level,'w')
outp.write('# Taxonomic Profiling Output\n')
outp.write('@SampleID:metaphlan2\n')
outp.write('@Version:0.9.1\n')
outp.write('@Ranks:superkingdom|phylum|class|order|family|genus|species\n')
outp.write('@@TAXID\tRANK\tTAXPATH\tTAXPATHSN\tPERCENTAGE\n')
ranks = {}
rank_name = {'species': 's__', 'order': 'o__', 'genus': 'g__', 'family': 'f__', 'class': 'c__', 'phylum': 'p__', 'kingdom': 'k__', 'domain': 'k__'}
rank_abr =  {'s__': 'species', 'o__': 'order', 'g__': 'genus', 'f__': 'family', 'c__': 'class', 'p__': 'phylum', 'k__': 'kingdom', 'd__': 'domain'}
for line in open('/bbx/tmp/profiled_metagenome.txt'):
    if line.startswith('#'):
        continue
    try:
        line.split('\t')[0].split(rank_name[level])[1].split('|')[1]
    except IndexError:
        taxid = None
        try:
            taxid = list(ncbi.get_name_translator([line.split('\t')[0].split('__')[-1]]).values())[0][0]
        except IndexError:
            try:
                taxid = list(ncbi.get_name_translator([line.split('\t')[0].split('__')[-1].replace('_',' ')]).values())[0][0]
            except IndexError:
                print('%s not found' % line.split('\t')[0].split('__')[-1].replace('_',' '))
        if taxid:
            outp.write('%s\t%s\t%s\t%s\t%s' % (taxid, rank_abr[line.split('|')[-1][0:3]],taxid,taxid,line.split('\t')[1]))

